<h2>ANZ Netball Championship Visualisation</h2>
<p>This project is a visualisation of all games and related data for the ANZ Netball Championships.</p>

<p>Live version here: <a href"http://johnarmstrong.co/uni">ANZ Visualisation</a></p>
<p>This was my assignment for SWEN303 - User Interface Design at Victoria University 2014</p>
